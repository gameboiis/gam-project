﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;		//Allows us to use Lists. 
using UnityEngine.UI;					//Allows us to use UI.
	
/// <summary>
/// This class is used as the overall game handler. 
/// </summary>
public class GameManager : MonoBehaviour{
	public static GameManager instance = null;				//Static instance of GameManager which allows it to be accessed by any other script.
	[HideInInspector] public bool playersTurn = true;		//Boolean to check if it's players turn, hidden in inspector but public.
	private bool doingSetup;
	
	/// <summary>
	/// 
	/// </summary>
	void Awake(){ 
		//Check if instance already exists
		if (instance == null) 
			//if not, set instance to this
            instance = this;
		//If instance already exists and it's not this:
        else if (instance != this)
			//Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
			Destroy(gameObject);	
			
		//Sets this to not be destroyed when reloading scene
		DontDestroyOnLoad(gameObject);
			
		//Assign enemies to a new List of Enemy objects.
		//enemies = new List<Enemy>();
			
		//Get a component reference to the attached BoardManager script
		//boardScript = GetComponent<BoardManager>();
			
		//Call the InitGame function to initialize the first level 
		InitGame();
	}

	//this is called only once, and the paramter tell it to be called only after the scene was loaded
    //(otherwise, our Scene Load callback would be called the very first load, and we don't want that)
    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
    static public void CallbackInitialization(){
	    //register the callback to be called everytime the scene is loaded
		SceneManager.sceneLoaded += OnSceneLoaded;
	}

	//This is called each time a scene is loaded.
	static private void OnSceneLoaded(Scene arg0, LoadSceneMode arg1){ 
		instance.InitGame();
	}

		
	//Initializes the game for each level.
	void InitGame(){
		//While doingSetup is true the player can't move, prevent player from moving while title card is up.
		//doingSetup = true;
		//doingSetup = false;
	}

		//GameOver is called when the player reaches 0 food points
	public void GameOver(){
		//Disable this GameManager.
		enabled = false;
	}
		
	//Coroutine to move enemies in sequence.
	IEnumerator MoveEnemies(){
		return null;
	}
}

